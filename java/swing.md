# Swing

Is used by many old Java desktop applications.

- Set platform look and feel: https://docs.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
- HTML browser
- extract all JDialogs from JFrames into separate classes, maybe even so for important JPanels
- tips (http://www.alethis.net/reference/java/swing.html)
- threads (https://stackabuse.com/how-to-use-threads-in-java-swing/)
- JFileChooser do they need to be cached in some way or is it okay to create them on the fly?

### JFrame

- setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
- setLocationByPlatform(true);

### Browser

- JEditorPane is awfully behind (https://stackoverflow.com/questions/22579965/browser-using-jeditorpane-forcing-blue-background)
  JavaFX recommended as alternative

### Better Property (for ResourceBundles/options for example)

### GraphicsUtils

- public Icon getAsIcon(...)