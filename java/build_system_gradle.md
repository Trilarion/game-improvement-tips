# Build system (Gradle)

- IntelliJ Gradle project uses JUNit5. Often not needed. See included build.gradle file. gradle/wrapper/jar properties determines gradle version (bin/all)
- obey folder structure src/[main/test]/[java/resources]
- Gradle uses the system JDK. [Latest Gradle version](https://docs.gradle.org/current/userguide/installation.html#sec:running_and_testing_your_installation)
- IntelliJ settings/build/tools/gradle check use-gradle-from-build-wrapper