# Deployment

## Inbuilt package and then generic installer

- Usage of the inbuilt Packager (https://medium.com/@adam_carroll/java-packager-with-jdk11-31b3d620f4a8).
- Uses jlink
- adds quite a lot of size (100 MB) --> only good for big projects
  
## Izpack

- http://izpack.org/
- documentation (https://izpack.atlassian.net/wiki/spaces/IZPACK/pages/491528/IzPack+5)
- better for small projects

## Others

- https://github.com/puniverse/capsule
- https://github.com/threerings/getdown
- https://github.com/fvarrui/JavaPackager
- https://github.com/cilki/jlink.online
- https://github.com/libgdx/packr