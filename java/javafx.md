# JavaFX

- https://docs.oracle.com/javafx/2/best_practices/jfxpub-best_practices.htm
- https://github.com/AlmasB/FXGL - widgets
- https://github.com/PatMartin/Dex - data explorer
- https://github.com/mhrimaz/AwesomeJavaFX - lists

## Conversion from Swing

- https://stackoverflow.com/questions/35906779/converting-from-swing-to-javafx
- https://blog.idrsolutions.com/2015/02/converting-swing-application-javafx-listeners-fast-scrolling/
- https://www.cs.mcgill.ca/~martin/blog/2018-11-12.html
- https://docs.oracle.com/javase/8/javafx/interoperability-tutorial/port-to-javafx.htm
- https://docs.oracle.com/javafx/2/swing/port-to-javafx.htm
- https://docs.oracle.com/javafx/2/swing/jtable-barchart.htm
- https://docs.oracle.com/javafx/2/swing/jfxpub-swing.htm#sthref2