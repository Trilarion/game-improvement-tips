package xxx;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * Immutable 2D Vector of ints.
 */
public class VectorInt2D implements Serializable {

    public static final VectorInt2D ZERO = new VectorInt2D(0, 0);
    private static final long serialVersionUID = -383973807383372775L;
    public final int x, y;

    public VectorInt2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static VectorInt2D add(@NotNull VectorInt2D a, @NotNull VectorInt2D b) {
        return new VectorInt2D(a.x + b.x, a.y + b.y);
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static VectorInt2D subtract(@NotNull VectorInt2D a, @NotNull VectorInt2D b) {
        return new VectorInt2D(a.x - b.x, a.y - b.y);
    }

    /**
     * @param v
     * @param x
     * @return
     */
    public static VectorInt2D newX(@NotNull VectorInt2D v, int x) {
        return new VectorInt2D(x, v.y);
    }

    /**
     * @param v
     * @param y
     * @return
     */
    public static VectorInt2D newY(@NotNull VectorInt2D v, int y) {
        return new VectorInt2D(v.x, y);
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof VectorInt2D)) return false;

        final VectorInt2D other = (VectorInt2D) obj;

        return x == other.x && y == other.y;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        return x * 29 + y;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
