package xxx;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * Immutable 2D Vector of doubles.
 */
public class VectorDouble2D implements Serializable {

    public static final VectorDouble2D ZERO = new VectorDouble2D(0, 0);
    private static final long serialVersionUID = -4246923796359812746L;
    public final double x, y;

    public VectorDouble2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static VectorDouble2D add(@NotNull VectorDouble2D a, @NotNull VectorDouble2D b) {
        return new VectorDouble2D(a.x + b.x, a.y + b.y);
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static VectorDouble2D subtract(@NotNull VectorDouble2D a, @NotNull VectorDouble2D b) {
        return new VectorDouble2D(a.x - b.x, a.y - b.y);
    }

    /**
     * @param v
     * @param x
     * @return
     */
    public static VectorDouble2D newX(@NotNull VectorDouble2D v, double x) {
        return new VectorDouble2D(x, v.y);
    }

    /**
     * @param v
     * @param y
     * @return
     */
    public static VectorDouble2D newY(@NotNull VectorDouble2D v, double y) {
        return new VectorDouble2D(v.x, y);
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof VectorDouble2D)) return false;

        final VectorDouble2D other = (VectorDouble2D) obj;

        return x == other.x && y == other.y;
    }

    /**
     * @return
     */
    @Override
    public double hashCode() {
        return x * 29 + y;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
