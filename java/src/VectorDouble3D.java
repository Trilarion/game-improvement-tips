package xxx;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * Immutable 3D Vector of doubles.
 */
public class VectorDouble3D implements Serializable {

    public static final VectorDouble3D ZERO = new VectorDouble3D(0, 0, 0);
    public final double x, y, z;

    public VectorDouble3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static VectorDouble3D add(@NotNull VectorDouble3D a, @NotNull VectorDouble3D b) {
        return new VectorDouble3D(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static VectorDouble3D subtract(@NotNull VectorDouble3D a, @NotNull VectorDouble3D b) {
        return new VectorDouble3D(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    /**
     * @param v
     * @param x
     * @return
     */
    public static VectorDouble3D newX(@NotNull VectorDouble3D v, double x) {
        return new VectorDouble3D(x, v.y, v.z);
    }

    /**
     * @param v
     * @param y
     * @return
     */
    public static VectorDouble3D newY(@NotNull VectorDouble3D v, double y) {
        return new VectorDouble3D(v.x, y, v.z);
    }

    /**
     * @param v
     * @param z
     * @return
     */
    public static VectorDouble3D newZ(@NotNull VectorDouble3D v, double z) {
        return new VectorDouble3D(v.x, v.y, z);
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof VectorDouble3D)) return false;

        final VectorDouble3D other = (VectorDouble3D) obj;

        return x == other.x && y == other.y && z == other.z;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        return (Double.hashCode(x) * 29 + Double.hashCode((y))) * 31 + Double.hashCode(z);
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}
