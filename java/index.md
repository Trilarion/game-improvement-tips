# Java

- JDK from AdoptOpenJDK (LTS 11 release and LTS 8 release), in IntelliJ set Project settings/SDK to JDK and then set Gradle JDK to use project SDK (https://stackoverflow.com/questions/18487406/how-do-i-tell-gradle-to-use-specific-jdk-version)
- package names are org.game-name.XXX and resources are in data/..

- [Build System Gradle](build_system_gradle.md)
- [Swing](swing.md)
- [JavaFX](javafx.md)
- [IDE IntelliJ](ide_intellij.md)
- [Deployment](deployment.md)

## General Java tips

- getResource(file) searches for main/java/resources/package/file
- convert static final ints to enums with IntelliJ refactor/extract delegate/extract as enum (problem: int often used for indexing in arrays, use maps instead)
- java.util.Observer/Obserable is deprecated and can easily be replaced by a type-safe custom implementation (https://stackabuse.com/the-observer-design-pattern-in-java/)
- prefer immutable objects, especially in the model
- recommended main packages: model, ui, controller, io, utils, ai
- convert static methods to instance methods where suitable (to avoid non-null checks for example)
- rename variable to longer names (example: Move m -> Move move)
- encapsulate public fields and use getters/setters (except in the same class)
- extract interfaces
- hash and equals and order implementations (see Object.equals)
- singletons
- Vector and array of non-primitive types can be replaced by ArrayList (https://stackoverflow.com/questions/322715/when-to-use-linkedlist-over-arraylist-in-java), HashTable by HashMap (https://stackoverflow.com/questions/40471/what-are-the-differences-between-a-hashmap-and-a-hashtable-in-java)
- make all classes public after moving inner classes to outer scope and before sorting into packages
- recommended order is public static final (https://stackoverflow.com/questions/16731240/what-is-a-reasonable-order-of-java-modifiers-abstract-final-public-static-e)
- check for headless graphics environment (https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/java/awt/GraphicsEnvironment.html#isHeadless())
- immutable variables (final) of simple classes; all others, getter and setter
- https://stackoverflow.com/questions/1066589/iterate-through-a-hashmap, https://stackoverflow.com/questions/46898/how-do-i-efficiently-iterate-over-each-entry-in-a-java-map (keyset, entryset, values)
- https://stackoverflow.com/questions/997482/does-java-support-default-parameter-values
- https://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
- https://stackoverflow.com/questions/1568091/why-use-getters-and-setters-accessors
- https://stackoverflow.com/questions/529085/how-to-create-a-generic-array-in-java
- https://stackoverflow.com/questions/2591098/how-to-parse-json-in-java
- https://stackoverflow.com/questions/4963300/which-notnull-java-annotation-should-i-use
- https://stackoverflow.com/questions/20001427/double-colon-operator-in-java-8
- https://stackoverflow.com/questions/2671496/when-to-use-static-methods
- https://stackoverflow.com/questions/299659/whats-the-difference-between-softreference-and-weakreference-in-java
- https://stackoverflow.com/questions/70689/what-is-an-efficient-way-to-implement-a-singleton-pattern-in-java
- https://stackoverflow.com/questions/26684562/whats-the-difference-between-map-and-flatmap-methods-in-java-8
- https://stackoverflow.com/questions/4343202/difference-between-super-t-and-extends-t-in-java
- https://stackoverflow.com/questions/2723397/what-is-pecs-producer-extends-consumer-super (? extends..)
  
- tips (http://www.alethis.net/reference/java/java.html)
- best practices (https://medium.com/@yazanghandour/java-best-practices-afc90e612004) + SOLID principle
- best practices (https://www.codejava.net/coding/10-java-core-best-practices-every-java-programmer-should-know)
- best practices (https://docs.oracle.com/cd/A97688_16/generic.903/bp/java.htm, https://docs.oracle.com/cd/A97688_16/generic.903/bp/toc.htm)
- design patterns (https://java-design-patterns.com/patterns/)
- https://www.softwaretestinghelp.com/design-patterns-in-java/
- https://javarevisited.blogspot.com/

### Deprecated warnings

- HashTable -> Map / HashMap
- Vector -> List / ArrayList
- Stack -> Deque / ArrayDeque

### Workflow

- do reformatting
- do uncritical static analyzes improvements
- move inner classes out
- make all classes public
- sort into packages (preliminary), reduce package dependencies
- do more critical static analyzes
  * deprecated usage
  * raw types 
  * array with generics to lists
  * vector to arraylist
  * hashtable to hashmap
  * check static methods
  * only private variables with getters/setters, those without setters become final
  * search for "for (Object)", look for type casts nearby, then inspection(data flow) inline redundant local variables
  * List sometimes mixed with java.awt.List
  
#### nD array (int, double) and linear array with indices

#### Pair<A, B>

see also https://stackoverflow.com/questions/457629/how-to-return-multiple-objects-from-a-java-method

#### 2D, 3D Vector of int, double

## Cached resources

- cached resources (generic? effective java)
- https://stackoverflow.com/questions/299659/whats-the-difference-between-softreference-and-weakreference-in-java

## Internationalization

- use ResourceBundle (https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/ResourceBundle.html)
- all the other things (format of numbers, dates, ... https://www.ibm.com/developerworks/java/tutorials/j-i18n/j-i18n.html)

## Reflection

- to detect use of reflection in Java code, search for java.lang.Class or .getClass() or java.lang.reflect.
- https://stackoverflow.com/questions/37628/what-is-reflection-and-why-is-it-useful

## Serialization

- https://www.baeldung.com/jackson-vs-gson
- https://stackabuse.com/reading-and-writing-json-in-java/
- https://github.com/google/gson (gson)
- https://stackoverflow.com/questions/285793/what-is-a-serialversionuid-and-why-should-i-use-it

## Scripting

- https://docs.oracle.com/javase/8/docs/technotes/guides/scripting/prog_guide/api.html

## Sound

- Ogg files (https://stackoverflow.com/questions/24764960/how-to-play-ogg-files-in-java, https://github.com/nnttoo/OggJavaPlayer, https://github.com/wrldwzrd89/audio-ogg, https://github.com/goxr3plus/java-stream-player)

## Persistent user configuration

- user.home system property (https://stackoverflow.com/questions/194349/what-is-the-proper-way-to-store-apps-conf-data-in-java)
- user properties and serialize to/from properties (https://www.techrepublic.com/article/store-your-apps-configuration-information-with-java-properties/)

## Logging

- log4j
- private static final Logger LOG = Logger.getLogger(Loader.class.getName());
- search for System.out.println and replace with LOG formats
- https://tinylog.org/v2/

## Network communication

- https://javaee.github.io/grizzly/
- http://mina.apache.org/

## Concurrency

- https://github.com/JCTools/JCTools additional concurrent data structures
- https://github.com/jobrunr/jobrunr - background processing
- https://github.com/quartz-scheduler/quartz - job scheduler
- https://github.com/Coreoz/Wisp - job scheduler
- https://github.com/lukas-krecan/ShedLock - distributed lock?
- https://github.com/code-review-checklists/java-concurrency - checklist
- https://stackoverflow.com/questions/541487/implements-runnable-vs-extends-thread-in-java

### Messaging

- https://github.com/LMAX-Exchange/disruptor

## Design principles

- [Is encapsulation still one of the elephants OOP stands on?](https://softwareengineering.stackexchange.com/questions/358611/is-encapsulation-still-one-of-the-elephants-oop-stands-on)
- [How do you avoid getters and setters?](https://softwareengineering.stackexchange.com/questions/284215/how-do-you-avoid-getters-and-setters)
- more on https://softwareengineering.stackexchange.com/questions/tagged/java?tab=votes&page=2&pagesize=30
- https://github.com/iluwatar/java-design-patterns

## Libraries/engine

- http://jbox2d.org/ - Physics engine
- http://fastutil.di.unimi.it/ - fast collections
- https://labs.carrotsearch.com/hppc.html - fast primitives
  https://github.com/leventov/Koloboke - collections

- https://jmonkeyengine.org/ - game engine
- https://libgdx.badlogicgames.com/ - cross-plattform
- https://www.lwjgl.org/ - game engine
- https://mini2dx.org/ (really Java?)

## Awesome Java lists

- [A curated list of awesome Java frameworks, libraries and software.](https://github.com/akullpp/awesome-java) most stars, also at https://githublists.com/lists/akullpp/awesome-java
- [Awesome Java 8](https://github.com/tedyoung/awesome-java8)
- [Amazing Java list](https://github.com/wtsxDev/Amazing-Java-List)
- [Awesome Java](https://github.com/pditommaso/awesome-java)