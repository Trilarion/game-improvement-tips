# IDE (IntelliJ)

- IntelliJ ([community edition](https://www.jetbrains.com/idea/download/)) is the recommended IDE
- License headers in settings/editor/add copyright profile (
  https://medium.com/@shan1024/managing-copyright-notices-in-intellij-idea-a3f0456267ba)
- see gpl3.txt
- then add scope and connect to copyright (file[XXX.main]:***.java for all java files)
- right click on folder/update copyright
- Code style: Settings/Editor/Code Style/Java
- Custom dictionary: Settings/Editor/Proofreading/Spelling/Add custom dictionaries
- TODO right click on found + expand all, group by (nothing), navigate with single click
- internationalization support in IDE (?) maybe edits of resourcebundles?
- settings/file encodings/ src/main/java should be utf-8
- How to prevent open last projects when IntelliJ IDEA starts? (https://stackoverflow.com/questions/5362036/how-to-prevent-open-last-projects-when-intellij-idea-starts)
- count lines of code with IntelliJ plugin (Statistic)