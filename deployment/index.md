# Deployment

- OSGH (open source game hub)
- only for those that do not have their own download capability or are improved

## What information is required?

- Name
- Single line description
- Screenshot (1-3)
- Tags  
- Link to more info (OSGL entry, Official Homepage, Official Source)

Releases
- Version
- Single line version description
- Publication date of version
- Supported OS
- License information  
- Download link for each supported OS (File name should encode name, OS, version, publication date)

- include source code version for every release as zipped snapshot of the git repository

### How is the information stored and changed

YAML or JSON and a simple editor for it

## Sorting options

- by supported OS
- by main tags
- by name
- search within description

## other features

- download counter tracker
- advertisement (possibly, only if more than 100 downloads per day on average and not very intrusive)