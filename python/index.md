# Python

- recommended IDE is PyCharm
- recommended environment is conda/pip/pip-review
- Conda Cheat-Sheet https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf
- single environments for every project

## Build System

requirements.txt and setup.py

## Documentation with sphinx and restructured text

- Sphinx doc (https://www.sphinx-doc.org/en/master/)
- install sphinx with conda or pip
- cd to target directory
- call "sphinx-quickstart"
- call "make html"
- restructured text tips at https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html

### Internationalization with Sphinx

- https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-internationalization

## Deployment

- on PyPI ?

## Conversion Python 2 to 3