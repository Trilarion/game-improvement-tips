# Game improvement tips

Collection of wisdom/best practices/tips in order to [systematically improve open source games](https://trilarion.blogspot.com/2020/12/how-to-improve-open-source-games.html).

- [Java](java/index.md)
- [Pascal](pascal/index.md)
- [JavaScript](javascript/index.md)
- [Python](python/index.md)
- [C](c/index.md)
- [C++](cpp/index.md)
- [Artwork](artwork/index.md)

## General steps

- Check and complete the entry in the OSGL (https://github.com/Trilarion/opensourcegames)
- Check the code license. Only start working if the code license is unmistakably clear open source.
- Getting the game to (compile and) run somehow. Fix major crashes that might occur because of the time passed since the game was abandoned.
  This is the most difficult step.
- Improve and convert the building system (Ant, Maven to Gradle, make to CMake)
- Get the tests running smoothly.
- Adapt README and write a HISTORY
- Maybe convert code license from MIT, Apache, BSD to GPL
- Do a first code cleanup, remove unused code, remove unused dependencies
- Check the artwork license. Remove all artwork without a clear free license. Replace with alternatives if possible.
- Update the programming language version / compiler version (Python 2 to 3, Java 8 to 11)
- Thoroughly test the game and make a list of possible improvements. Prioritize them by effort and benefit.
- Implement the high benefit, low effort improvements.
- Minimize the number of dependencies as much as possible without major replacements.
- Update dependencies (SDL to SDL2 for example).
- Increase platform support if possible.
- Do a final code cleanup.
- Improve documentation and leave a list of possible further improvements.
- Put on GitHub (if previous attempts already on GitHub or very popular) or GitLab if not yet there.
- Make a release and distribute it
- Post about it on social media

## Git wisdom

- script ./user_gitlab.sh or ./user_github.sh 
- git config --global diff.algorithm histogram
- git config --global diff.renamelimit 20000

## Licensing migration

- https://drewdevault.com/2019/06/13/My-journey-from-MIT-to-GPL.html
- https://opensource.stackexchange.com/questions/6062/using-gpl-library-with-mit-licensed-code (do I need two license headers)
- https://softwareengineering.stackexchange.com/questions/105912/can-you-change-code-distributed-under-the-mit-license-and-re-distribute-it-unde
- https://stackoverflow.com/questions/3902754/mit-vs-gpl-license

### General resources

- [Awesome lists](https://github.com/topics/awesome-list?o=desc&s=stars)
- https://github.com/mgreiler/code-review-checklist

## Games in progress

### Java 

- [FreeRails](https://github.com/Trilarion/freerails)
- [AntiChess](https://gitlab.com/Trilarion/antichess)
- [Spice Trade](https://gitlab.com/Trilarion/spicetrade)
- [4D Maze](https://gitlab.com/Trilarion/4d-maze)
- [Domino on Acid](https://gitlab.com/Trilarion/domino-on-acid)